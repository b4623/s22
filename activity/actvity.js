{
	_id: UserID,
	firstName: String,
	latName: String,
	email: String,
	password: String,
	isAdmin: false,
	mobileNo: String
}

{
	_id: OrderId,
	UserID: UserId,
	transactionDate: Date,
	status: String,
	totalAmount: Number
}
{
	_id: ProductId,
	name: String,
	description: String,
	price: Number,
	stocks: Number,
	isActive: true,
}